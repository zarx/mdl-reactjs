module.exports = {
  Badge: require('./components/Badge'),
  Button: require('./components/Button'),
  Card: require('./components/Card'),
  CardActions: require('./components/CardActions'),
  CardMedia: require('./components/CardMedia'),
  CardSupportingText: require('./components/CardSupportingText'),
  CardTitle: require('./components/CardTitle'),
  Menu: require('./components/Menu'),
  MenuItem: require('./components/MenuItem'),
  Progress: require('./components/Progress'),
  Slider: require('./components/Slider'),
  Spinner: require('./components/Spinner'),
  Textfield: require('./components/Textfield'),
  asdf: ''
    /*
    Badge: require('./lib/Badge'),
    Card: require('./lib/Card'),
    Checkbox: require('./lib/Checkbox'),
    DataTable: require('./lib/DataTable'),
    FABButton: require('./lib/FABButton'),
    Icon: require('./lib/Icon'),
    IconButton: require('./lib/IconButton'),
    IconToggle: require('./lib/IconToggle'),
    ProgressBar: require('./lib/ProgressBar'),
    Radio: require('./lib/Radio'),
    RadioGroup: require('./lib/RadioGroup'),
    Slider: require('./lib/Slider'),
    Spinner: require('./lib/Spinner'),
    Switch: require('./lib/Switch'),
    Textfield: require('./lib/Textfield'),
    Tooltip: require('./lib/Tooltip')
    */
};
